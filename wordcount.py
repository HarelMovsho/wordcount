import sys
import re

def word_list(filename):
    with open(filename, "r") as file:
        data = file.read().replace('\n', ' ')
    regex = re.compile("[,:*.!?]")
    data = regex.sub('', data)
    filewords = data.lower().split()
    words = []
    for word in filewords:
        if word not in [lis[0] for lis in words]:
            words.append((word, data.lower().count(word)))
    return words


def print_words(filename, flag):
        words = word_list(filename)
        if flag == "--letter":
            words.sort()
        if flag == "--appearance":
            words.sort(reverse=True, key=lambda x: x[1])
        for word in words:
          print(word[0] + " " + str(word[1]))


def print_top(filename):
        words = word_list(filename)
        words.sort(reverse=True, key = lambda x: x[1])
        if len(words) > 20:
            for x in range(20):
                print(words[x][0] + " " + str(words[x][1]))
        else:
            for word in words:
                print(word[0] + " " + str(word[1]))

def main():
    if len(sys.argv) != 3 and len(sys.argv) != 4:
        print("usage: ./wordcount.py {--count {--letter | --appearance} | ---topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    if option == '--count':
        flag = sys.argv[2]
        if flag == '--letter' or flag == '--appearance':
            filename = sys.argv[3]
            print_words(filename, flag)
        elif not flag.endswith('.txt'):
            print("File not supported: " + flag)
            sys.exit(1)
        else:
            filename = sys.argv[2]
            print_words(filename, "--letter")
    elif option == '--topcount':
        filename = sys.argv[2]
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()
